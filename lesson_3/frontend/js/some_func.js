// Задача:
// Написать ф-ю которая принимает на вход обьект с сервера и
// разбить его на 3 массива по параметрам описаным ниже.
// + бонус вывести каждый список на экран
// + бонус 2 сделать поле инпута куда вставить ссылку с json-generator
// для перерендера списка по клику на кнопку
// + бонус 3 если вставить не валидную ссылку выводить ошибку

// # = условие -> вывод
// array 1 = fruit == banana -> name,
// array 2 = balance > 2000, age > 25
// array 3 = eyeColor === blue, gender === female, isActive === false
/*
var array_1 = [], array_2 = [], array_3 = [], link, input, button, users, baseBalance, IsArray, testArray, IsURL, testURL, listArray_1, listArray_2, listArray_3;

button = document.querySelector('.btn-secondary');
link = document.getElementById('json_link');
var error = document.querySelector('.is-invalid');

link.addEventListener ("click", (e) => {
    var target = e && e.target || event.srcElement;
    var href = target.innerHTML;
    input = document.getElementById('jsonGenerator');
    input.removeAttribute('value');
    input.setAttribute('value',href);
    e.preventDefault();
    return false;
});

button.addEventListener ("click", () => {
    input = document.getElementById('jsonGenerator').value;

    IsURL = (url)=> {
        var objRE = /(http(s)?)/gi ;
        return objRE.test(url);
    };

    testURL = IsURL(input);
    console.log(testURL);

    async function MyApp(){
        baseBalance = '2000.00';
        
        const getUserResponse = await fetch(input);
        const users = await getUserResponse.json();

        console.log('=============Start - banana==================');

        let fruit = users.filter((item, i, arr) => {
            return arr[i].favoriteFruit === 'banana';
        });

        for (var j = 0; j < fruit.length; j++) {
            array_1.push(fruit[j].name);
        }
        console.log(array_1);

        listArray_1 = ()=> {
            for (var i = 0; i < array_1.length; i++) {
                let newlist = document.createElement("li");
                newlist.innerHTML = array_1[i];
                document.getElementById("list_array_1").appendChild(newlist);
            }
        };

        listArray_1();

        console.log('=============Start - > 25 > $2000.00==================');

        let balances = users.filter((item, i, arr) => {
            var itemBalsnce = arr[i].balance;
            var resultBalance = itemBalsnce.replace(/[^.\d]+/g,"").replace( /^([^\.]*\.)|\./g, '$1' );
            return arr[i].age > '25' && resultBalance > baseBalance;
        });
        for (var j = 0; j < balances.length; j++) {
            array_2.push(balances[j].name);
        }
        console.log(array_2);

        listArray_2 = ()=> {
            for (var i = 0; i < array_2.length; i++) {
                let newlist = document.createElement("li");
                newlist.innerHTML = array_2[i];
                document.getElementById("list_array_2").appendChild(newlist);
            }
        }

        listArray_2();

        console.log('=============Start - blue - female - false==================');

        let eyeFemaleIsActive = users.filter((item, i, arr) => {
            return arr[i].eyeColor === 'blue' && arr[i].gender === 'female' && arr[i].isActive === false;
        });
        for (var j = 0; j < eyeFemaleIsActive.length; j++) {
            array_3.push(eyeFemaleIsActive[j].name);
        }
        console.log(array_3);

        listArray_3 = ()=> {
            for (var i = 0; i < array_3.length; i++) {
                let newlist = document.createElement("li");
                newlist.innerHTML = array_3[i];
                document.getElementById("list_array_3").appendChild(newlist);
            }
        };

        listArray_3();

        /!*const CombinedUser = {
            name: selectedUserName,
            friends: UserFriends[0].friends
        };
        return CombinedUser;
        return rand;*!/
    }

    MyApp();

   /!* if(!testURL === false) {
        async function MyApp(){
            const getUserResponse = await fetch(input);
            const users = await getUserResponse.json();
            console.log(users);

            /!*const CombinedUser = {
                name: selectedUserName,
                friends: UserFriends[0].friends
            };
            return CombinedUser;
            return rand;*!/
        }

        MyApp();

        // (function MyApp(){
        //     // console.log('init function');
        //     // https://www.json-generator.com/#
        //     var fetchedData = fetch(input).then(function(response) {
        //         return response.json();
        //     }).then(data => {
        //         renderInterface(data);
        //     });
        //
        // }());

       /!* function renderInterface( data ){
            users = data;

            IsArray = (str)=> {

                for (var i = 0; i < str.length; i++) {
                    if (str[i].favoriteFruit && str[i].balance && str[i].eyeColor && str[i].gender && str[i].isActive) {
                        return true;
                    }
                    return false;
                }
            };

            testArray = IsArray(users);
            console.log(testArray);

            if(!testArray === false) {
                baseBalance = '2000.00';

                console.log('=============Start - banana==================');

                let fruit = users.filter((item, i, arr) => {
                    return arr[i].favoriteFruit === 'banana';
                });

                for (var j = 0; j < fruit.length; j++) {
                    array_1.push(fruit[j].name);
                }
                console.log(array_1);

                listArray_1 = ()=> {
                    for (var i = 0; i < array_1.length; i++) {
                        let newlist = document.createElement("li");
                        newlist.innerHTML = array_1[i];
                        document.getElementById("list_array_1").appendChild(newlist);
                    }
                };

                listArray_1();

                console.log('=============Start - > 25 > $2000.00==================');

                let balances = users.filter((item, i, arr) => {
                    var itemBalsnce = arr[i].balance;
                    var resultBalance = itemBalsnce.replace(/[^.\d]+/g,"").replace( /^([^\.]*\.)|\./g, '$1' );
                    return arr[i].age > '25' && resultBalance > baseBalance;
                });
                for (var j = 0; j < balances.length; j++) {
                    array_2.push(balances[j].name);
                }
                console.log(array_2);

                listArray_2 = ()=> {
                    for (var i = 0; i < array_2.length; i++) {
                        let newlist = document.createElement("li");
                        newlist.innerHTML = array_2[i];
                        document.getElementById("list_array_2").appendChild(newlist);
                    }
                }

                listArray_2();

                console.log('=============Start - blue - female - false==================');

                let eyeFemaleIsActive = users.filter((item, i, arr) => {
                    return arr[i].eyeColor === 'blue' && arr[i].gender === 'female' && arr[i].isActive === false;
                });
                for (var j = 0; j < eyeFemaleIsActive.length; j++) {
                    array_3.push(eyeFemaleIsActive[j].name);
                }
                console.log(array_3);

                listArray_3 = ()=> {
                    for (var i = 0; i < array_3.length; i++) {
                        let newlist = document.createElement("li");
                        newlist.innerHTML = array_3[i];
                        document.getElementById("list_array_3").appendChild(newlist);
                    }
                };

                listArray_3();
                input = document.getElementById('jsonGenerator');
                input.style.borderColor="rgba(0, 0, 0, 0.15)";
            } else {
                input = document.getElementById('jsonGenerator');
                input.style.borderColor="red";
                alert('No data to check')
            }
        }*!/

        error.style.display='none';
        input = document.getElementById('jsonGenerator');
        input.style.borderColor="rgba(0, 0, 0, 0.15)";
    } else {
        input = document.getElementById('jsonGenerator');
        input.style.borderColor="red";
        error.style.display='block'
    }*!/
});*/


/*
function data(datat,cat,summ) {
    this.datat = datat;
    this.cat = cat;
    this.summ = summ;
    this.info = function () {
        console.log('data:', this.datat);
        console.log('categoriya:', this.cat);
        console.log('summ:', this.summ);
    };
    this.list = function () {
        for (let prop in purchase ) {
            console.log('--------------');
            console.log(prop);
        }
    };
}

var purchase = new data('2017/11/14','продукты','2000');
purchase.info();
purchase.list();*/

/*
let data = {
    waterG: '86',
    waterH: '13,85',
    waterO: '3,96'
};

function water(userWaterG, userWaterH, userWaterO) {
    this.userWaterG = userWaterG * data.waterG;
    this.userWaterH = userWaterH * data.waterH;
    this.userWaterO = userWaterO * data.waterO;
    
}*/

let data = {
    animal:'',
    sound: ''
};

var animal = {
    tolk: function (data) {
        let{animal,sound} = this;
        console.log(animal + ' sey ' + sound);
    }
};

var cat = Object.setPrototypeOf(data,animal);
cat.tolk('gor', "woff");