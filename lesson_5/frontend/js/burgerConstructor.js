

  /*

    Задание:

      1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню

      Дожно выглядеть так:
      new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);

      Прототип для бургера:
      {
        cookingTime: 0,     // Время на готовку
        showComposition: function(){
          let {composition, name} = this;
          let compositionLength = composition.length;
          console.log(compositionLength);
          if( compositionLength !== 0){
            composition.map( function( item ){
                console.log( 'Состав бургера', name, item );
            })
          }
        }
      }

      Результатом конструктора нужно вывести массив меню c добавленными элементами.
      // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]

        2. Создать конструктор заказов

        Прототип:
        {
          id: "",
          orderNumber: "",
          orderBurder: "",
          orderException: "",
          orderAvailability: ""
        }

          Заказ может быть 3х типов:
            1. В котором указано название бургера
              new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
              new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
            3. В котором указано чего не должно быть
              new Order('', 'except', 'Название ингредиента') ->


            Каждый их которых должен вернуть статус:
            "Заказ 1: Бургер ${Название}, будет готов через ${Время}

            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
              new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
              Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ
              Если нет, предложить еще вариант из меню.

        3. Протестировать программу.
          1. Вначале добавляем наши бургеры в меню (3-4 шт);
          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
          3. Формируем 3 заказа

        Бонусные задания:
        4. Добавлять в исключения\пожелания можно несколько ингредиентов
        5. MEGABONUS
          Сделать графическую оболочку для программы.

  */

  var the_burger,new_york, macho_nacho, handy_man, williams_burger,the_uncle_sam,kentucky,carolina,indiana,custom;
  var  btn, burgerWrap,burgerName, burgerComposition;

  const ingredients_menu = [
      ['томат', 'бекон', 'салат айсберг', 'маринованный огурец', 'сыр чеддер', 'соус барбекю', 'майонез', 'лук фри', 'котлета из говядины', 'булочка с сезамом'],
      ['томат', 'салат айсберг', 'маринованный огурец', 'соус барбекю', 'майонез', 'красный лук', 'две котлеты из говядины', 'булочка с сезамом', 'двойной сыр чеддер'],
      ['томат', 'салат айсберг', 'сыр эмменталь', 'перец халапеньо', 'маслины', 'красный лук', 'котлета из говядины', 'булочка с сезамом', 'начос'],
      ['томат', 'бекон', 'салат айсберг', 'кетчуп', 'майонез', 'красный лук', 'булочка с сезамом', 'двойная котлета', 'двойной сыр чеддер'],
      ['бекон', 'маринованный огурец', 'сыр чеддер', 'сыр эмменталь', 'кетчуп', 'булочка с сезамом', 'двойная котлета', 'лук тушеный', 'соус чеддер'],
      ['томат', 'горчица', 'салат айсберг', 'сыр гауда', 'кетчуп', 'майонез', 'красный лук', 'котлета из говядины', 'булочка с сезамом'],
      ['томат','сыр моцарелла', 'соус песто', 'салат айсберг', 'куриное филе', 'булочка с сезамом'],
      ['салат айсберг','соус дор-блю','утиная грудка','слайсы яблока карамелизированные','булочка с кунжутом','фирменный ягодный соу'],
      ['салат айсберг', 'соус цезарь', 'сыр чеддер', 'карамелизированные яблоки', 'котлета из индейки', 'булочка с сезамом'],
  ];

  const add_ingredients = [
    'Темная булка',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Перепилиные яйца',
    'Двойной сыр',
  ];

  const composition = [
      'Булка',
      'Огурчик',
      'Котлетка',
      'Бекон',
      'Рыбная котлета',
      'Соус карри',
      'Кисло-сладкий соус',
      'Помидорка',
      'Маслины',
      'Острый перец',
      'Капуста',
      'Кунжут',
      'Сыр Чеддер',
      'Сыр Виолла',
      'Сыр Гауда',
      'Майонез'
  ];

  var OurMenu = [];
  var OurOrders = [];

  the_burger ={
        name: 'The burger',
        composition: ingredients_menu[0],
        cookingTime: '20',
        id: '1'
  };
  new_york = {
        name: 'New York',
        composition: ingredients_menu[1],
        cookingTime: '25',
        id: '2'
  };
  macho_nacho = {
        name: 'Macho Nacho',
        composition: ingredients_menu[2],
        cookingTime: '15',
        id: '3'
  };
  handy_man = {
        name: 'The Uncle Sam',
        composition: ingredients_menu[3],
        cookingTime: '15',
        id: '4'
  };
  williams_burger = {
        name: 'Handy Man',
        composition: ingredients_menu[4],
        cookingTime: '19',
        id: '5'
  };
  the_uncle_sam = {
        name: 'Williams burger',
        composition: ingredients_menu[5],
        cookingTime: '24',
        id: '6'
  };
  kentucky = {
        name: 'Kentucky',
        composition: ingredients_menu[6],
        cookingTime: '14',
        id: '7'
  };
  carolina = {
        name: 'Carolina',
        composition: ingredients_menu[7],
        cookingTime: '10',
        id: '8'
  };
  indiana = {
        name: 'Indiana',
        composition: ingredients_menu[8],
        cookingTime: '30',
        id: '9'
  };
  custom = {
      name: 'Custom',
      ingredients: '',
      cookingTime: '10',     // Время на готовку
      id: '10',             // Индификатор
      showComposition: function(){
          let {ingredients,cookingTime, name} = this;
          let compositionLength = ingredients.length;
          let compositions = ingredients;
          console.log( 'Название бургера:',  name);
          console.log( 'Время готовки', cookingTime );
          console.log( 'Количество ингредиентов бургера:', compositionLength);
          if( compositionLength !== 0){
              compositions.map( function( item ){
                  console.log( 'Состав бургера:', item );
              })
          }
      }
  };

  OurMenu.push(the_burger,new_york,macho_nacho,handy_man,williams_burger,the_uncle_sam,kentucky,carolina,indiana);

  console.log(OurMenu);

  function burgerMenu( name, ingredients, cookingTime, id){

      custom.name = name;
      custom.cookingTime = cookingTime;
      custom.ingredients = ingredients;
      custom.id = id;
      custom.showComposition();

      // console.log( 'Название бургера',  name );
      // console.log( '------------------------------');
      // console.log( 'Состав бургера', ingredients );
      // console.log( '------------------------------');
      // console.log( 'Время готовки', cookingTime );

      return {name,ingredients, cookingTime};
  };

  function Order(name, condition, value){

  };

  Order()

  function sectionBurger() {
      for(let i =0; OurMenu.length > i; i++){
          let burgerText = document.querySelector('.block_text');
          let additem = document.createElement("div");
          additem.className = "burger-wrap";
          burgerText.appendChild(additem);

          let addName = document.createElement("div");
          addName.className = "burger-name";
          additem.appendChild(addName).innerHTML = `${OurMenu[i].name}`;

          let addComposition = document.createElement("div");
          addComposition.className = "burger-composition";
          additem.appendChild(addComposition).innerHTML = `<span>Состав: </span>  ${OurMenu[i].composition}`;

          let addbtn = document.createElement("div");
          addbtn.className = "burger-add btn";
          addbtn.setAttribute('data-burger-name', OurMenu[i].name);
          additem.appendChild(addbtn).innerHTML = `Заказать`;
      }
  }
  sectionBurger();

  function youOrder() {
      btn = document.querySelectorAll('.burger-add');
      let buttons, result, order, NameOrder;
      let ourOrderName = document.querySelector('.our_order_name');
      let ourOrderTime = document.querySelector('.our_order_time');
      let btnOrder = document.querySelector('#btn_order');

      for (let i = 0; i <  btn.length; i++) {
           btn[i].addEventListener ("click", (event) => {
               buttons = event.target.getAttribute("data-burger-name");

               function GetItemName(name){
                   for(let i=0;i<=OurMenu.length;i++){
                       if(OurMenu[i].name==name){
                           return OurMenu[i];
                       }
                   }
                   return null;
               };
               result = GetItemName(buttons);
               OurOrders.push(result);
               console.log(`Ваш заказ ${OurOrders}`);
               function SetItemName() {
                   for(let i=0;i<=result.length;i++){
                       if(result[i] !== undefined){
                           return result[i].name;
                       }
                   }
                   return null;
               }
               // NameOrder = SetItemName();
               btnOrder.style = 'block';
               ourOrderName.innerHTML = result.name;
               ourOrderTime.innerHTML = `Время приготовления: ${result.cookingTime} минут`;
          });
      }
  };
  youOrder();


  function sortYes() {
      let ingredientsYes = document.querySelector('#ingredients_yes');
      let burgerSearch = document.querySelector('#burgerSearch');


      burgerSearch.addEventListener ("click", (event) => {
          let inputIngredientYes = ingredientsYes.value;

         if(inputIngredientYes.length > 0 ) {
             let inputIngredientsYes = inputIngredientYes.toLowerCase().split(',');

         }

      });
  }

  sortYes();

  function sortNo() {
      let ingredientsNo = document.querySelector('#ingredients_no');
      let burgerSearch = document.querySelector('#burgerSearch');


      burgerSearch.addEventListener ("click", (event) => {
          let inputIngredientNo = ingredientsNo.value;

          if(ingredientsNo.length > 0 ) {
              let inputIngredientsYes = ingredientsNo.toLowerCase().split(',');

          }

      });
  }

  sortNo();


