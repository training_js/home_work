/*

  Модули в JS
  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import

  Так как для экспорта и импорта нету родной поддержки в
  браузерах, то нам понадобится сборщик который это умеет делать

  Выбор пал на вебпак.
  npm install --save-dev webpack

  Создадим файл конфига webpack.config.js

  База с документации
  const path = require('path');
  module.exports = {
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    }
  };

  Затестим - в консоли наберем команду webpack
*/

// Для экспорта и импорта обязательно нужно имя
import str from './modules/1.js';
console.log(str);

import someObj from './modules/2';
console.log(someObj);

import { mArray, mObj, User } from './modules/3';
console.log( mArray, mObj, User);

const Butch = new User('Butch');
console.log(Butch);

console.log('1da2net3');

/*

  Установка webpack-dev-server
  npm install --save-dev webpack-dev-server
  -> автообвновления и пересборка


  package.json ->
    scripts -> "start": "webpack-dev-server --open"

  webpack.config.js ->
    devServer: {
      contentBase: './public'
    }

*/


/*
  Установим babel
  npm install babel-loader babel-core babel-preset-env --save-dev

  usage: https://webpack.js.org/loaders/babel-loader/#usage
  webpack.config.js ->
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    ]
  }

*/

/*
  Задание - разбить задание про птиц на модули и собрать в бандл при помощи вебпака.
*/
